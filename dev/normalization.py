import numpy as np
def normalize_zero_abs_one(x):
    x_mean = np.mean(x)
    x_min, x_max = x.min(), x.max()
    diff = x_max - x_min
    normalized = (x - x_mean) / diff
    return normalized, diff, x_mean

def normalize_standard(x):
    x_mean, x_std = np.mean(x), np.std(x)
    normalized = (x - x_mean) / x_std
    return normalized, x_std, x_mean

def normalize_zero_to_one(x):
    x_min, x_max = x.min(), x.max()
    diff = x_max - x_min
    normalized = (x - x_min) / diff

    return normalized, diff, x_min