import numpy as np
from activation import Softmax

log_epsilon=1e-05
class Loss(object):
    def __init__(self):
        pass
    
class L2Loss(Loss):
    def dC(self, Y, y):
        n = Y.shape[0]
        m = Y.shape[1]
        return (Y - y)/(m*n)

    def __call__(self, Y, y):
        n = Y.shape[0]
        m = Y.shape[1]
        result = np.power(np.subtract(Y.flat,y.flat), 2).sum()/(2*m*n)
        return result

# y -> True (contains hot encoded labels), Y-> predicted


# https://deepnotes.io/softmax-crossentropy
# https://towardsdatascience.com/derivative-of-the-softmax-function-and-the-categorical-cross-entropy-loss-ffceefc081d1
# https://levelup.gitconnected.com/killer-combo-softmax-and-cross-entropy-5907442f60ba
class CrossEntropyLoss(Loss):
    def __init__(self):
        super().__init__()

    def __call__(self, a, y):     
        m = y.shape[1]
        n = y.shape[0]
        y_c = 1 - y
        y_a = 1 - a
        return -(np.dot(y.flat, np.log(a.flat)) + np.dot(y_c.flat, np.log(y_a.flat)))/(m * n)

    def dC(self, a, y):
        m = y.shape[1]
        n = y.shape[0]
        return -(y - y/(1-a))/(m * n)
