import numpy as np
from copy import copy
from sklearn.metrics import r2_score

class ConstantLearningRate:
    def __init__(self, rate):
        self.__rate__ = rate

    def __float__(self):
        return self.__rate__

    def update(self, dict):
        pass

class SGD(object):
    def __init__(self, loss, pipeline, learning_rate=ConstantLearningRate(0.05)):        
        self.__pipeline__ = pipeline
        self.__learning_rate__ = learning_rate
        self.__loss__ = loss
        
    def dW(self, d0, d):
        return np.matmul(d0, d.T)

    def forward(self, X):        
        pipeline = self.__pipeline__
        return pipeline.forward(X)

    def dC(self, Y, y):
        loss = self.__loss__
        return loss.dC(Y,y)

    def backward(self, X, dC):     
        m = X.shape[1]
        learning_rate = float(self.__learning_rate__)
        pipeline = self.__pipeline__
        layers = pipeline.weight_layers
        layer_index = len(layers) - 1
        last_layer = layers[layer_index]        
        d0 = np.multiply(dC, last_layer.derivative) * learning_rate
        dW = self.dW(d0, layers[layer_index - 1].output)        
        last_layer.weights[:] = last_layer.weights - dW
        last_layer.biases[:] = last_layer.biases - d0.sum(1, keepdims=True)
        while layer_index > 0:
            
            prev_layer = layers[layer_index - 1]
            d0 = np.multiply(np.matmul(last_layer.weights.T, d0), prev_layer.derivative) * learning_rate
            a = layers[layer_index - 2].output if layer_index > 1 else X
            dW = self.dW(d0, a)        
            prev_layer.weights[:] = prev_layer.weights - dW
            prev_layer.biases[:] = prev_layer.biases - d0.sum(1, keepdims=True)
            last_layer = prev_layer
            layer_index -= 1   

    @property
    def pipeline(self):
        return copy(self.__pipeline__)

    def step(self, X, y, X_test, Y_test, iterations):  
        Z = X
        iteration_loss = float('inf')
        iteration_test_loss = iteration_loss
        train_loss = iteration_loss
        test_loss = iteration_loss
        y_pred = None
        optimal = None
        for i in range(0, iterations):            
            Y = self.forward(Z)   
            dC = self.dC(Y, y)              
            self.backward(Z, dC)
            iteration_loss = self.__loss__(Y,y)  
            
            if iteration_loss < train_loss:                   
                y_pred = self.__pipeline__.forward(X_test)
                iteration_test_loss = self.__loss__(Y_test, y_pred)
                train_loss = iteration_loss

                if iteration_test_loss <  test_loss:
                    test_loss = iteration_test_loss
                    optimal = copy(self.__pipeline__)
            

        return train_loss, test_loss, optimal, y_pred
    
