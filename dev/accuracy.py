import numpy as np

coeff_of_variation_impl = lambda x: np.std(x) / np.mean(x)
coeff_of_variation = lambda X, axis: np.apply_along_axis(coeff_of_variation_impl, axis=axis, arr=X)
pct_diff = lambda a, b: np.abs(np.abs(a-b) / ((a + b)) / 2)
def chi_square(y_pred, y_test): # https://www.geeksforgeeks.org/python-pearsons-chi-square-test/
    y_pred_flattened = y_pred.flatten()
    ratios = np.power(np.add(y_pred_flattened, -1*y_test.flatten()), 2) * (1 / y_pred_flattened)
    return ratios.sum()
