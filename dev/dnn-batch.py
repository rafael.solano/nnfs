from optimizer import ConstantLearningRate
import numpy as np
from activation import Softmax, Linear, Tanh, Relu
from layer import normal_bias_initializer, normal_weights_initializer, xavier_bias_initializer, xavier_weights_initializer, chi2_weights_initializer, chi2_bias_initializer
from pipeline import Pipeline
from loss import L2Loss, CrossEntropyLoss
from optimizer import SGD
from copy import copy
from normalization import normalize_zero_to_one, normalize_zero_abs_one
import pandas as pd
from scipy import stats
from accuracy import chi_square

def test_quasi_linear_regression_single_output():
    epochs = 100
    test_loss = 100
    alpha = 0.001
    min_test_loss = 0.009
    test_loss = 100
    train_loss = 100
    optimal_model_epoch = 0
    chi_test_alpha = 0.05
    csv = pd.read_csv('data/4x100oscilator.csv')
    X_set = (csv.iloc[: , :4]).to_numpy()
    Y_set = (csv['Y']).to_numpy()
    Y_set.shape = (Y_set.shape[0],1)
    batch_length = int(X_set.shape[0]*0.80)
    X_normalized, _, _ =  normalize_zero_abs_one(X_set)    
    Y_normalized, y_factor, y_bias = normalize_zero_abs_one(Y_set)  
    X_train = X_normalized[:batch_length]       
    X_test = X_normalized[batch_length:]     
    Y_train = Y_normalized[:batch_length]
    Y_test = Y_normalized[batch_length:]        
    loss = L2Loss()
    pipeline = Pipeline(
        (X_train.shape[1], 64, 64, 64, 64, Y_train.shape[1]),
        (Tanh(), Tanh(), Tanh(), Tanh(), Linear()),
        xavier_weights_initializer,
        xavier_bias_initializer
    )           
    optimal = pipeline

    optimizer = SGD(loss, pipeline, ConstantLearningRate(alpha))
    for epoch in range(0, epochs):  
        indices = np.random.randint(0, batch_length, batch_length) 
        x = np.take(X_train, indices, axis = 0)
        y = np.take(Y_train, indices, axis = 0)        

        (train_loss, test_loss, final_optimal, y_pred) = optimizer.step( x.T, y.T, X_test.T, Y_test.T, 100)            
        print('epoch: %d, train loss= %5.8f , test loss = %5.8f' %(epoch, train_loss, test_loss))
        if test_loss < min_test_loss:
            optimal_model_epoch = epoch
            break;

    y_test = Y_set[batch_length:]
    y_pred = y_pred * y_factor + y_bias
    chi_stat = chi_square(y_pred, y_test)
    # sf --> Area to the right of critical value (https://www.statisticshowto.com/tables/chi-squared-table-right-tail/)
    # Chi-Square Test --> https://www.westga.edu/academics/research/vrc/assets/docs/ChiSquareTest_LectureNotes.pdf
    p_value = stats.chi2.sf(chi_stat, y_test.shape[0] - 1) 
    y_stack = np.column_stack((y_pred.flat, y_test.flat))
    print(
        'optimal: %d, test loss= %5.8f, chiq=%8.5f, p-value=%8.5f, df=%d. y_test and y_pred %s to the same distribution' %(
            optimal_model_epoch, 
            test_loss     ,
            chi_stat,
            p_value,
            y_test.shape[0] - 1,
            'DO NOT belong' if p_value < chi_test_alpha else 'belong'
        )
    )
    print(y_stack)


def shuffle(x,y, times=1):
    length = x.shape[0]
    for i in range(times):
        indices = np.random.randint(0, length, length) 
        x = np.take(x, indices, axis = 0)
        y = np.take(y, indices, axis = 0) 
    return x,y

def test_quasi_linear_regression_multiple_output():
    epochs = 100
    test_loss = 100
    alpha = 0.005
    min_test_loss = 0.009
    test_loss = 100
    train_loss = 100
    optimal_model_epoch = 0
    chi_test_alpha = 0.05
    csv = pd.read_csv('data/4x100oscilator2D.csv')
    X_set, Y_set = shuffle((csv[['X1', 'X2', 'X3', 'X4']]).to_numpy(), (csv[['Y1','Y2']]).to_numpy(), len(csv))

    batch_length = int(X_set.shape[0]*0.80)
    X_train = X_set[:batch_length]       
    X_test = X_set[batch_length:]     
    Y_train = Y_set[:batch_length]
    Y_test = Y_set[batch_length:]        
    loss = L2Loss()
    
    pipeline = Pipeline(
        (X_train.shape[1], 12, 12, 12, Y_train.shape[1]),
        (Linear(), Linear(), Linear(), Linear()),
        chi2_weights_initializer,
        chi2_bias_initializer
    )           
    optimal = pipeline

    optimizer = SGD(loss, pipeline, ConstantLearningRate(alpha))
    for epoch in range(0, epochs):  
        indices = np.random.randint(0, batch_length, batch_length) 
        x = np.take(X_train, indices, axis = 0)
        y = np.take(Y_train, indices, axis = 0)        

        (train_loss, test_loss, final_optimal, y_pred) = optimizer.step( x.T, y.T, X_test.T, Y_test.T, 100)            
        print('epoch: %d, train loss= %5.8f , test loss = %5.8f' %(epoch, train_loss, test_loss))
        if test_loss < min_test_loss:
            optimal_model_epoch = epoch
            break;

    y_test = Y_test
    chi_stat = chi_square(y_pred, y_test)
    # sf --> Area to the right of critical value (https://www.statisticshowto.com/tables/chi-squared-table-right-tail/)
    # Chi-Square Test --> https://www.westga.edu/academics/research/vrc/assets/docs/ChiSquareTest_LectureNotes.pdf
    p_value = stats.chi2.sf(chi_stat, y_test.shape[0] - 1) 
    y_stack = np.concatenate((y_pred.T, y_test),1)
    print(
        'optimal: %d, test loss= %5.8f, chiq=%8.5f, p-value=%8.5f, df=%d. y_test and y_pred %s to the same distribution' %(
            optimal_model_epoch, 
            test_loss     ,
            chi_stat,
            p_value,
            y_test.shape[0] - 1,
            'DO NOT belong' if p_value < chi_test_alpha else 'belong'
        )
    )
    print(y_stack)

def one_hot_encode(Y):
    cat_0 = (1,0,0,0)
    cat_1 = (0,1,0,0)
    cat_2 = (0,0,1,0)
    cat_3 = (0,0,0,1)

    def one_hot_encode_row(row):
        if row[0] > 0:
            return cat_0
        if row[1] > 0:
            return cat_1
        if row[2] > 0:
            return cat_2
        return cat_3

    result = np.apply_along_axis(one_hot_encode_row, 1, Y)
    return result

def test_multiple_categories():
    epochs = 100
    test_loss = 100
    alpha = 5e-8
    min_test_loss = 0.009
    test_loss = 100
    train_loss = 100
    optimal_model_epoch = 0
    chi_test_alpha = 0.05
    csv = pd.read_csv('data/4x100categories.csv')
    X_set, Y_set = shuffle((csv[['X']]).to_numpy(), one_hot_encode(csv[['Y1','Y2','Y3']]), len(csv)+100)

    batch_length = int(X_set.shape[0]*0.80)
    X_normalized = X_set
    Y_normalized = Y_set
    X_train = X_normalized[:batch_length]       
    X_test = X_normalized[batch_length:]     
    Y_train = Y_normalized[:batch_length]
    Y_test = Y_normalized[batch_length:]        
    loss = CrossEntropyLoss()
    
    pipeline = Pipeline(
        (X_train.shape[1], 32, 32, 32, Y_train.shape[1]),
        (Relu(), Relu(), Relu(), Softmax()), 
        xavier_weights_initializer,
        xavier_bias_initializer
    )           
    
    optimal = pipeline
    
    for epoch in range(0, epochs):  
        optimizer = SGD(loss, pipeline, ConstantLearningRate(alpha))
        indices = np.random.randint(0, batch_length, batch_length) 
        x = np.take(X_train, indices, axis = 0)
        y = np.take(Y_train, indices, axis = 0)        

        (train_loss, test_loss, final_optimal, y_pred) = optimizer.step( x.T, y.T, X_test.T, Y_test.T, 10)   
        accuracy = np.mean(np.argmax(y_pred.T, 1) == np.argmax(Y_test, 1))
        print('epoch: %d, train loss= %5.8f , test loss = %5.8f, accuracy=%5.8f' %(epoch, train_loss, test_loss, accuracy))        
        print(np.argmax(y_pred.T, 1))
        print(np.argmax(Y_test, 1))
        if accuracy > 0.70:
            optimal_model_epoch = epoch
            break;


if __name__ == "__main__":    
    np.random.rand(0)
    with np.printoptions(precision=10, suppress=True):
        # test_quasi_linear_regression_single_output()
        test_quasi_linear_regression_multiple_output()
        # test_multiple_categories()
       