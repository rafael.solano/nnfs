import numpy as np
class Activation(object):
    @property
    def pool(self):
        return self.__pool__

    def __call__(self, x):
        pass

def relu_implementation(X):
    return np.array(list(map(lambda x: max(0.0, x), X)))

def relu_derivative_implementation(X):
    return np.array(list(map(lambda x: 0.0 if x < 0.0 else 1.0, X)))
    
class Relu(Activation):
    def __init__(self):
        super().__init__()
        self.__state__ = None
        self.__derivative__ = None

    def __call__(self, X):        
        self.__state__ = np.apply_along_axis(relu_implementation, 0, X)
        self.__derivative__ = np.apply_along_axis(relu_derivative_implementation, 0, X)
        return self.__state__

    @property
    def derivative(self):
        return self.__derivative__
    
class Tanh(Activation):
    def __init__(self):
        super().__init__()
        self.__state__ = None
        self.__derivative__ = None

    def __call__(self, X):         
        self.__state__  = np.tanh(X)
        self.__derivative__ =  1 - np.power(X, 2)
        return self.__state__

    @property
    def derivative(self):
        return self.__derivative__

class Linear(Activation):
    def __init__(self):
        super().__init__()
        self.__state__ = None
        self.__derivative__ = None

    def __call__(self, X):        
        self.__state__ = np.apply_along_axis(lambda x: x, 0, X)
        self.__derivative__ = np.apply_along_axis(lambda x: 1, 0, X)
        return self.__state__

    @property
    def derivative(self):
        return self.__derivative__

# https://towardsdatascience.com/derivative-of-the-softmax-function-and-the-categorical-cross-entropy-loss-ffceefc081d1        
def softmax_derivative_implementation(X, i):   
    return np.array(tuple(map(lambda k: X[i]*(1-X[i]) if i == k else -X[i]*X[k], range(len(X)))))

class Softmax(Activation):
    def __init__(self):
        super().__init__()
        self.__state__ = None
        self.__derivative__ = None
        self.__sum__ = 0

    def __call__(self, X): 
        e_x = np.exp(X - np.max(X))       
        self.__sum__ = e_x.sum()
        self.__state__ = e_x / self.__sum__
        self.__derivative__ = np.multiply(self.__state__, (1-self.__state__))
        return self.__state__

    @property
    def output(self):
        return self.__state__

    @property
    def derivative(self):
        return self.__derivative__
