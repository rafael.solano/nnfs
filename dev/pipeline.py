from layer import Layer, normal_weights_initializer, normal_bias_initializer
import collections.abc
from copy import copy

import numpy as np
class Pipeline(object):    
    def __init__(self, layers, activation, weights_initializer, bias_initializer, drop_rate = 0):
        if not (layers is None):
            length = len(layers)
            self.__weight_layers__ = []
            self.__output__ = None

            for next_layer in range(0, length-1):
                if isinstance(activation, collections.abc.Sized):
                    activation_instance = activation[next_layer]
                else:
                    activation_instance = activation

                self.__weight_layers__.append(
                    Layer(layers[next_layer], layers[next_layer+1], activation_instance, weights_initializer, bias_initializer, drop_rate)
                )
            self.__weight_layers__ = tuple(self.__weight_layers__)

    def __len__(self):
        return len(self.__weight_layers__)

    def __copy__(self):
        weight_layers = tuple([copy(layer) for layer in self.__weight_layers__])
        output = np.copy(self.__output__) if not (self.__output__ is None) else None
        cloned = Pipeline(None, None, None, None)
        cloned.__weight_layers__ = weight_layers
        cloned.__output__ =output
        return cloned

    @property
    def output(self):
        return self.__output__

    @property
    def layers(self):
        return self.__weight_layers__

    @property
    def weight_layers(self):
        return self.__weight_layers__

    def forward(self, X):
        layers = self.layers
        length = len(layers)
        input = X

        for index in range(0, length):
            layer = layers[index]
            input = layer.activation(layer.forward(input))
        self.__output__ = input
        return self.__output__
