import numpy as np
from sampling import primes1D
normal_weights_initializer = lambda inputs, neurons: np.random.normal(
    size=inputs * neurons)

normal_bias_initializer = lambda neurons: np.random.normal(size=neurons)


chi2_weights_initializer = lambda inputs, neurons: np.power(np.random.normal(
    size=inputs * neurons),2)

chi2_bias_initializer = lambda neurons: np.power(np.random.normal(size=neurons),2)

primes_weights_initializer = lambda inputs, neurons: primes1D(inputs * neurons)

primes_bias_initializer = lambda neurons: primes1D(1 * neurons)

# Tanh and Sigmoid
def he_weights_initializer(inputs, neurons):
    weights = normal_weights_initializer(inputs, neurons)
    return weights * np.sqrt(2/inputs)

def he_bias_initializer(neurons):
    bias = normal_bias_initializer(neurons)
    return bias * np.sqrt(2/neurons)

def xavier_weights_initializer(inputs, neurons):
    weights = np.random.uniform(-1, 1, inputs * neurons)
    return weights * np.sqrt(6/(inputs + neurons))

def xavier_bias_initializer(neurons):
    bias = np.random.uniform(-1, 1, neurons)
    return bias * np.sqrt(6/neurons)

class Layer(object):
    def __init__(self, features, neurons, activation, weights_initializer, bias_initializer, drop_rate):
        if not (features is None):
            self.__weights__ = weights_initializer(features, neurons)
            self.__weights__.resize((neurons, features))
            self.__dropouts__ = np.ones_like(self.__weights__)
            self.__activation__ = activation
            self.__biases__ = bias_initializer(neurons)
            self.__biases__.resize((neurons, 1))
            self.__output__ = None
            self.__ds__ = None
            self.__drop_rate__ = drop_rate

    def __copy__(self):
        cloned = Layer(None, None, None, None, None, 0)
        cloned.__weights__ = np.copy(self.__weights__)
        cloned.__dropouts__ = np.ones_like(self.__weights__)
        cloned.__activation__ = self.__activation__
        cloned.__biases__ = np.copy(self.__biases__)
        cloned.__output__ = np.copy(self.__output__) if not (self.__output__ is None) else None
        cloned.__ds__ = np.copy(self.__ds__) if not (self.__ds__ is None) else None
        cloned.__drop_rate__ = self.__drop_rate__
        return cloned
    
    def forward(self, inputs):
        if self.__drop_rate__ > 0.0:
            shape = self.__weights__.shape
            self.__dropouts__[:] = 1
            linear_size = shape[0]*shape[1]
            drop_size = int(linear_size * self.__drop_rate__)            
            if drop_size > 0:
                drop_indices = np.random.choice(linear_size, size=drop_size, replace=False)
                self.__weights__.flat[drop_indices] = 0  

        self.__output__ = self.__activation__(
            np.matmul(np.multiply(self.__dropouts__,self.__weights__), inputs) + self.__biases__)
        self.__ds__ = self.__activation__.derivative
        return self.output

    @property
    def activation(self):
        return self.__activation__

    @property
    def output(self):
        return self.__output__

    @property
    def derivative(self):
        return self.__ds__

    @property
    def shape(self):
        return self.__weights__.shape

    @property
    def weights(self):
        return self.__weights__

    @property
    def biases(self):
        return self.__biases__
     