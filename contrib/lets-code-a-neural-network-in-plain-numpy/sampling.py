import numpy as np
import random
from primesset import PRIMES_SET


def validate_sample_size(rows, columns):
    length = rows * columns
    if length > len(PRIMES_SET):
        raise ValueError(
            f"Requested sample is too big: {rows} * {columns} > {len(PRIMES_SET)}"
        )

def contigousIntegers(rows, columns):
    validate_sample_size(rows, columns)
    result = np.array(range(0, rows * columns)) + 1
    result.shape = (rows, columns)
    return result

def gaussian2D(rows, columns, loc = 0, scale = 1):
    validate_sample_size(rows, columns)
    result = np.random.normal(loc, scale, rows * columns)
    result.shape = (rows, columns)
    return result

def primes1D(length):
    validate_sample_size(length, 1)
    result = np.array(PRIMES_SET[0:length])
    return result


def primes2D(rows, columns):
    validate_sample_size(rows, columns)
    result = np.array(PRIMES_SET[0:rows * columns])
    result.resize((rows, columns))
    return result


def primes2Drandom(rows, columns):
    validate_sample_size(rows, columns)
    result = np.array(random.sample(PRIMES_SET, rows * columns))
    result.resize((rows, columns))
    return result


def primes1Drandom(length):
    validate_sample_size(length, 1)
    result = np.array(random.sample(PRIMES_SET, length))
    return result if length > 1 else result[0]
