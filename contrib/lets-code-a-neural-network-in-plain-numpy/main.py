import numpy as np
from sampling import primes1Drandom

def copy_params(param_values):
    return {key: np.copy(param_values[key]) for key in param_values.keys()}

def init_layers(nn_architecture, seed=99):
    np.random.seed(seed)
    params_values = {}

    for idx, layer in enumerate(nn_architecture):
        layer_idx = idx + 1
        layer_input_size = layer["input_dim"]
        layer_output_size = layer["output_dim"]

        params_values['W' + str(layer_idx)] = np.random.normal(
            0, 1, (layer_output_size, layer_input_size)) * 0.1
        params_values['b' + str(layer_idx)] = np.random.normal(
            0, 1, (layer_output_size, 1)) * 0.1

    return params_values

        
def tanh(Z):
    return np.tanh(Z)

def tanh_backward(dA, Z):
    return dA * (1 - np.power(Z, 2))


def sigmoid(Z):
    return 1 / (1 + np.exp(-Z))


def sigmoid_backward(dA, Z):
    sig = sigmoid(Z)
    return dA * sig * (1 - sig)

def relu(Z):
    return np.maximum(0, Z)

def relu_backward(dA, Z):
    dZ = np.array(dA, copy=True)
    dZ[Z <= 0] = 0
    return dZ

def linear(Z):
    return Z

def linear_backward(dA, Z):
    return np.ones_like(Z)

def single_layer_forward_propagation(A_prev,
                                     W_curr,
                                     b_curr,
                                     activation="relu"):
    Z_curr = np.dot(W_curr, A_prev) + b_curr

    if activation == "relu":
        activation_func = relu
    elif activation == "sigmoid":
        activation_func = sigmoid
    elif activation == "tanh":
        activation_func = tanh
    elif activation == "linear":
        activation_func = linear
    else:
        raise Exception('Non-supported activation function')

    return activation_func(Z_curr), Z_curr


def full_forward_propagation(X, params_values, nn_architecture, dropout = None):
    dropout_impl = dropout if not (dropout is None) else lambda x: x
    memory = {}
    A_curr = X

    for idx, layer in enumerate(nn_architecture):
        layer_idx = idx + 1
        A_prev = A_curr

        activ_function_curr = layer["activation"]
        W_curr = dropout_impl(params_values["W" + str(layer_idx)])
        b_curr = dropout_impl(params_values["b" + str(layer_idx)])
        A_curr, Z_curr = single_layer_forward_propagation(
            A_prev, W_curr, b_curr, activ_function_curr)

        memory["A" + str(idx)] = A_prev
        memory["Z" + str(layer_idx)] = Z_curr

    return A_curr, memory


def log_cost_value(Y_hat, Y):
    m = Y_hat.shape[1]
    cost = -1 / m * (np.dot(Y,
                            np.log(Y_hat).T) + np.dot(1 - Y,
                                                      np.log(1 - Y_hat).T))
    return np.squeeze(cost)


def mse_cost_value(Y_hat, Y):
    assert np.array_equal(Y_hat.shape, Y.shape)
    m = Y_hat.shape[1]
    cost = (np.power(Y_hat - Y, 2) / m).sum()
    return cost


def convert_prob_into_signed_class(Y_hat):
    Y_hat_ = np.copy(Y_hat)
    Y_hat_[Y_hat > 0.0] = 1
    Y_hat_[Y_hat <= 0.0] = 0
    return Y_hat_


def binary_signed_accuracy_value(Y_hat, Y):
    assert np.array_equal(Y_hat.shape, Y.shape)
    Y_hat_ = convert_prob_into_signed_class(Y_hat)
    return (Y_hat_ == Y).all(axis=0).mean()


def single_layer_backward_propagation(dA_curr,
                                      W_curr,
                                      b_curr,
                                      Z_curr,
                                      A_prev,
                                      activation="relu"):
    m = A_prev.shape[1]

    if activation == "relu":
        backward_activation_func = relu_backward
    elif activation == "sigmoid":
        backward_activation_func = sigmoid_backward
    elif activation == "tanh":
        backward_activation_func = tanh_backward
    elif activation == "linear":
        backward_activation_func = linear_backward        
    else:
        raise Exception('Non-supported activation function')

    dZ_curr = backward_activation_func(dA_curr, Z_curr)
    dW_curr = np.dot(dZ_curr, A_prev.T) / m
    db_curr = np.sum(dZ_curr, axis=1, keepdims=True) / m
    dA_prev = np.dot(W_curr.T, dZ_curr)

    return dA_prev, dW_curr, db_curr

def full_backward_propagation(Y_hat, Y, memory, params_values,
                              nn_architecture):    
    grads_values = {}
    m = Y.shape[1]
    Y = Y.reshape(Y_hat.shape)

    dA_prev = -(np.divide(Y, Y_hat) - np.divide(1 - Y, 1 - Y_hat))

    for layer_idx_prev, layer in reversed(list(enumerate(nn_architecture))):
        layer_idx_curr = layer_idx_prev + 1
        activ_function_curr = layer["activation"]

        dA_curr = dA_prev

        A_prev = memory["A" + str(layer_idx_prev)]
        Z_curr = memory["Z" + str(layer_idx_curr)]
        W_curr = params_values["W" + str(layer_idx_curr)]
        b_curr = params_values["b" + str(layer_idx_curr)]

        dA_prev, dW_curr, db_curr = single_layer_backward_propagation(
            dA_curr, W_curr, b_curr, Z_curr, A_prev, activ_function_curr)

        grads_values["dW" + str(layer_idx_curr)] = dW_curr
        grads_values["db" + str(layer_idx_curr)] = db_curr

    return grads_values

def dropout(W):
    drop_rate = 0.1    
    shape = W.shape
    linear_size = shape[0]*shape[1]
    drop_size = int(linear_size * drop_rate)
    if drop_size > 0:
        drop_indices = np.random.choice(linear_size, size=drop_size, replace=False)
        W.flat[drop_indices] = 0    
    return W

def update(params_values, grads_values, nn_architecture, learning_rate):
    for idx, layer in enumerate(nn_architecture):
        layer_idx = idx + 1
        params_values["W" + str(
            layer_idx)] -= learning_rate * grads_values["dW" + str(layer_idx)]
        params_values["b" + str(
            layer_idx)] -= learning_rate * grads_values["db" + str(layer_idx)]
    return params_values


def train(X, Y, nn_architecture, params_values, cost_value, accuracy_value, epochs, learning_rate, dropout = None):    
    cost_history = []
    accuracy_history = [] if not (accuracy_value is cost_value) else cost_history

    for i in range(epochs):
        Y_hat, cashe = full_forward_propagation(X, params_values,
                                                nn_architecture, dropout)
        cost = cost_value(Y_hat, Y)
        cost_history.append(cost)

        if not (cost_value is accuracy_value):
            accuracy = accuracy_value(Y_hat, Y)
            accuracy_history.append(accuracy)

        grads_values = full_backward_propagation(Y_hat, Y, cashe,
                                                 params_values,
                                                 nn_architecture)
        params_values = update(params_values, grads_values, nn_architecture,
                               learning_rate)
    return np.array(cost_history), np.array(accuracy_history), params_values

def test_signed_binary_classification():
    nn_architecture = [
        {
            "input_dim": 2,
            "output_dim": 6,
            "activation": "tanh"
        },
        {
            "input_dim": 6,
            "output_dim": 6,
            "activation": "tanh"
        },
        {
            "input_dim": 6,
            "output_dim": 6,
            "activation": "tanh"
        },
        {
            "input_dim": 6,
            "output_dim": 6,
            "activation": "tanh"
        },
        {
            "input_dim": 6,
            "output_dim": 1,
            "activation": "tanh"
        },
    ]
    length = 90
    indices = np.random.randint(0, length, length)
    X = np.take(np.row_stack(
        tuple(map(lambda x: (x * 0.5, x * 0.75), range(0, length)))),
                indices,
                axis=0)
    Y = np.apply_along_axis(
        lambda x: -1 if np.cos(np.radians(x[0] + x[1])) < 0 else 1, 1, X)    
    Y.shape = (length, 1)    
    a = len(Y[Y < 1])
    b = length - a
    
    train_length = int(length * 0.8)
    X_train = X[:train_length]
    Y_train = Y[:train_length]
    X_test = X[train_length:]
    Y_test = Y[train_length:]

    print(min(a, b) / max(a, b))


    Y_train_transposed = Y_train.T
    X_train_transposed = X_train.T
    X_test_transposed = X_test.T
    Y_test_transposed  = Y_test.T    
    test_accuracy = 1

    for round in (0, 10):
        params_values = init_layers(nn_architecture, 2)
        for batch_index in range(0, 10):        
            indices = np.random.randint(0, Y_train_transposed.shape[1], Y_train_transposed.shape[1])
            X_train_transposed = np.take(X_train_transposed, indices, axis=1)
            Y_train_transposed = np.take(Y_train_transposed, indices, axis=1)

            cost_history, accuracy_history, params_values = train(
                X_train_transposed,
                Y_train_transposed,
                nn_architecture,
                params_values,
                mse_cost_value,
                binary_signed_accuracy_value,
                100,
                0.00001,
                dropout
            )
            accurate_indices = np.where(accuracy_history > 0.5)
            history = np.column_stack((cost_history, accuracy_history))
            assorted = np.take(history, accurate_indices, axis=0)[0]
            if len(assorted) > 0:
                sorted_indices = np.argsort(assorted[:, 1])
                last_history_entry = assorted[sorted_indices[-1]]
                if last_history_entry[1] > 0.7:
                    Y_hat, _ = full_forward_propagation(X_test_transposed, params_values, nn_architecture)
                    Y_train_transposed = Y_train.T
                    test_cost = mse_cost_value(Y_hat, Y_test_transposed)
                    test_accuracy = binary_signed_accuracy_value(Y_hat, Y_test_transposed)          
                    if test_accuracy > 0.8:
                        break
        if test_accuracy > 0.8:
            print('round/batch=%d/%d: final cost=%8.5f, final accuracy=%8.5f, test cost=%8.5f, test accuracy=%8.5f' %
                (round, batch_index, last_history_entry[0], last_history_entry[1], test_cost, test_accuracy))

def normalize_minmax(x):
    x_min, x_max = x.min(), x.max()
    diff = x_max - x_min
    normalized = (x - x_min) / diff

    return normalized, diff, x_min

def test_linear_regression():
    nn_architecture = [
        {
            "input_dim": 3,
            "output_dim": 200,
            "activation": "relu"
        },                
        {
            "input_dim": 200,
            "output_dim": 1,
            "activation": "linear"
        },
    ]
    length = 100
    indices = np.random.randint(0, length, length)
    rnd_gen = lambda: primes1Drandom(1)

    X = np.take(np.row_stack(
        tuple(map(lambda x: (rnd_gen(), rnd_gen(), rnd_gen()), range(0, length)))),
                indices,
                axis=0)
    Y = np.apply_along_axis(
        lambda x: (x[0] * x[1] + x[2]) * np.cos(np.radians(min(x[0], x[1], x[2]))), 1, X)    

    Y.shape = (Y.shape[0],1)
    train_length = int(length * 0.8)
    X_normalized, X_factor, X_bias = normalize_minmax(X)
    Y_normalized, Y_factor, Y_bias = normalize_minmax(Y)    
    X_train = X_normalized[:train_length]
    Y_train = Y_normalized[:train_length]
    X_test = X_normalized[train_length:]
    Y_test = Y_normalized[train_length:]

    X_train_transposed = X_train.T
    Y_train_transposed = Y_train.T    
    X_test_transposed = X_test.T
    Y_test_transposed  = Y_test.T    
    train_accuracy = 1000
    params_values = init_layers(nn_architecture)
    optimal_params_values = params_values

    for batch_index in range(0, 100):        

        indices = np.random.randint(0, Y_train_transposed.shape[1], Y_train_transposed.shape[1])
        X_train_transposed = np.take(X_train_transposed, indices, axis=1)
        Y_train_transposed = np.take(Y_train_transposed, indices, axis=1)        
        cost_history, accuracy_history, params_values = train(
            X_train_transposed,
            Y_train_transposed,
            nn_architecture,
            params_values,
            mse_cost_value,
            mse_cost_value,
            1000,
            0.00005,
            dropout
        )
        
        history = np.column_stack((cost_history, accuracy_history))
        history_accuracy = history[-1][0]
        if  history_accuracy > train_accuracy:
            Y_hat, _  = full_forward_propagation(X_test_transposed, optimal_params_values, nn_architecture)
            test_acuracy = mse_cost_value(Y_hat, Y_test_transposed)
            Yh = Y_hat.T * Y_factor + Y_factor
            Yt = Y_test * Y_factor + Y_factor             
            Yr = np.column_stack((Yh, Yt))
            print(Yr[: 10])
            print(test_acuracy, Yt.std() / Yt.mean())
            break 

        print('history acuracy=%8.5f' % (history_accuracy))
        train_accuracy = history_accuracy
        optimal_params_values = copy_params(params_values)
        
if __name__ == "__main__":
    # inputs (x,y): 2 complementary values between 0 and 1 (x + y == 1)
    # outpout z: -1 or 1. -1 if np.cos(np.radians(x + y)) < 0 else 1
    # findings:
    #   a) Accuracy decreases as dataset grows.
    #   b) 10% dropout increases convergence speed albeit test accuracy doesn't show significant improvement.
    # test_signed_binary_classification()
    test_linear_regression()
